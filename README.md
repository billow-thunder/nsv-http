# NativeScript-Vue Http

A wrapper for the default `http` module in NativeScript. It also fixes some quirks with Android and gzip.

### Installation

```shell
npm i @billow/nsv-http
```

### Usage

```javascript
// backend.js example.

import { Http } from "@billow/nsv-http"
import { isAndroid } from 'platform'
import { getString } from "application-settings" // Example Only

let http = new Http({

  // Configure a base url for all requests
  baseUrl: 'https://api.test.com',

  // Example headers, typically this is what we use when interacting with a Laravel Passport API.
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json;charset=utf-8',
    'Accept-Encoding': isAndroid ? 'identity' : 'gzip, deflate, br', // Android requests fail when the server is gzipping responses, this is a work around.
    'X-Requested-With': 'XMLHttpRequest',
    'Authorization': 'Bearer ' + getString('token', '')
  }
})

http.post('/path', {...}, successCallback, errorCallback)
http.get('/path', successCallback, errorCallback)

```

### As a Vue plugin

```javascript
import Http from '@billow/nsv-http'

Vue.use(Http, {

  // Configure a base url for all requests
  baseUrl: 'https://api.test.com',

  headers: {...}
})

methods: {
  loadUser() {
    this.$http.get('/user/1', content => {
      let responsePayload = content
    }, error => {
      // handle the error
    })
  },
  createUser() {
    this.$http.post('/user', {
      name: 'John',
      surname: 'Doe'
    }, content => {
      let responsePayload = content
    }, error => {
      // handle the error
    })
  }
}

```


### Updating the Authorization Header

```javascript
let http = new Http({...})

let token = '12345'

http.setAuthorizationHeader(`Bearer ${token}`)

```