import { request as httpRequest } from 'tns-core-modules/http'

export default class Http
{
  constructor(options = {}) {
    if(!options.headers) {
      console.log('MISSING REQUIRED OPTION: headers')
      return
    }
    this.headers = options.headers

    if(!options.baseUrl) {
      console.log('MISSING REQUIRED OPTION: baseUrl')
      return
    }

    this.baseUrl = options.baseUrl
  }

  setAuthorizationHeader(authType) {
    this.headers['Authorization'] = authType
  }

  setHeaders(headers) {
    this.headers = headers
  }

  succeeded(code) {
    return (code >= 200) && (code < 400)
  }

  request (method, context, success, failure) {
    console.dir(`HTTP REQUEST STARTING: ${method} - ${context.url}`)
    return httpRequest({
      method,
      url: this.baseUrl + context.url,
      content: (context.data) ? JSON.stringify(context.data) : undefined,
      headers: this.headers,
    })
      .then(response => {
        response.content = response.content.toJSON()
        console.dir('HTTP RESPONSE RECEIVED: ' + response.statusCode)
        return this.succeeded(response.statusCode)
          ? success(response)
          : failure(response)
      })
      .catch(error => {
        console.log('HTTP REQUEST FAILED: ' + error)
        return failure(error)
      })
  }

  post(url, data, success, failure) {
    return this.request('POST', { url, data }, success, failure)
  }

  get (url, success, failure) {
    return this.request('GET', { url }, success, failure)
  }
}