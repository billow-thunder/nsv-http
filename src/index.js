import _Http from './Http'

export let Http = _Http

let install = (Vue, options = {}) => {
  Vue.prototype.$http = new _Http(options)
}

export default {
  install
}
